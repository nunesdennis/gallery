//
//  PhotoSizeLoader.swift
//  Gallery
//
//  Created by Dennis Nunes on 24/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class PhotoDetailsListLoader: NetworkResquester {

	typealias ResultHandler = (_ photoDetailsList: PhotoDetailsList?,_ errorType: RequestErrorType, _ identifier: String?) -> Void
	private let resultHandler: ResultHandler
	
	init(resultHandler: @escaping ResultHandler) {
		self.resultHandler = resultHandler
	}
	
	func loadPhotoDetails(ofPhotoId id: String) {
		let networkManager = NetworkManager(networkRequester: self)
		
		let urlString = UrlGuide.photoGetDetailStringURL + "&photo_id=\(id)"
		
		if let url = URL(string: urlString) {
			networkManager.getRequest(withURL: url, withIdentifier: id)
		}
	}
	
	func handleResult(data: Data?, errorType: RequestErrorType, identifier: Any?) {
		guard let identifier = identifier as? String else {
			resultHandler(nil, .noError, nil)
			
			return
		}
		
		guard errorType == .noError else {
			resultHandler(nil, errorType, identifier)
			
			return
		}
		
		guard let data = data else {
			resultHandler(nil, errorType, identifier)
			
			return
		}
		
		let decoder = JSONDecoder()
		
		do {
			let photoDetailsSearch = try decoder.decode(PhotoDetailsSearch.self, from: data)
			let photoDetailsList = photoDetailsSearch.photoDetailsList

			resultHandler(photoDetailsList, .noError, identifier)
		} catch {
			resultHandler(nil, .parseError, identifier)
		}
	}
}
