//
//  PhotoListLoader.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class PhotoListLoader: NetworkResquester {

	typealias ResultHandler = (_ photoList: PhotoList?,_ errorType: RequestErrorType) -> Void
	private let resultHandler: ResultHandler
	
	init(resultHandler: @escaping ResultHandler) {
		self.resultHandler = resultHandler
	}
	
	func loadPhotoList(fromPage page: Int64=1,withTag tag: String="") {
		let networkManager = NetworkManager(networkRequester: self)
		
		let urlString = UrlGuide.photoSearchStringURL + "&tags=\(tag)" + "&page=\(page)"
		
		if let url = URL(string: urlString) {
			networkManager.getRequest(withURL: url)
		}
	}
	
	func handleResult(data: Data?, errorType: RequestErrorType, identifier: Any?=nil) {
		guard errorType == .noError else {
			resultHandler(nil, errorType)
			
			return
		}
		
		guard let data = data else {
			resultHandler(nil, errorType)
			
			return
		}
		
		let decoder = JSONDecoder()
		
		do {
			let photoSearch = try decoder.decode(PhotoSearch.self, from: data)
			let photoList = photoSearch.photoList
			
			resultHandler(photoList, .noError)
		} catch {
			resultHandler(nil, .parseError)
		}
	}
}
