//
//  GalleryViewModel.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class GalleryViewModel {
	
	private weak var controller: UICollectionViewController?
	private var allPhotos: [PhotoViewModel] = []
	private var currentTag = ""
  private var isLoadingPhotos = false
	private var currentPageNumber: Int64 = 0
	private var numberOfPages: Int64 = 0
	private var photosPerPage: Int = 100
	private var nextPage: Int64 {
		if numberOfPages > currentPageNumber {
			return currentPageNumber + 1
		}
		
		return numberOfPages
	}
	
  var needsFirstLoad: Bool {
    return allPhotos.count == 0
  }
  
  var numberOfSections: Int {
    return 1
  }
  
  var numberOfItemsInSection: Int {
    return allPhotos.count
  }
  
  init(collectionViewController: UICollectionViewController) {
    self.controller = collectionViewController
  }
  
	func photoCellSize(fromIndexPath indexPath: IndexPath) -> CGSize {
		return allPhotos[indexPath.row].photoSize
	}
	
	func loadGallery(withText text: String) {
		guard !text.isEmpty else { return }
		resetGallery()
		currentTag = text
		loadGallery()
  }
	
	func loadGallery(fromIndexPath indexPath: IndexPath? = nil) {
		guard !currentTag.isEmpty else { return }
		guard let indexPath = indexPath else {
			loadPhotoList()
			return
		}
		
		loadPhotoDetails(ofIndexPath: indexPath)
    
		guard shouldLoadMorePhotos(indexPath: indexPath) else { return }
		
    loadPhotoList()
  }
	
	private func loadPhotoDetails(ofIndexPath indexPath: IndexPath) {
		let photo = allPhotos[indexPath.row]
		guard photo.photoURL == nil else { return }
		
		let loader = PhotoDetailsListLoader(resultHandler: updatePhotoWithDetails(_:_:_:))
		let photoId = allPhotos[indexPath.row].photoId
		loader.loadPhotoDetails(ofPhotoId: photoId)
	}
	
	private func loadPhotoList() {
		isLoadingPhotos = true
    let loader = PhotoListLoader(resultHandler: updateUI(_:_:))
		loader.loadPhotoList(fromPage: nextPage, withTag: currentTag)
	}
	
	private func shouldLoadExtraPage(indexPath: IndexPath) -> Bool {
		let row = indexPath.row
		let visiblePage = Int64((row/photosPerPage) + 1)
		
		return (currentPageNumber - visiblePage) == 0
	}
	
  private func shouldLoadMorePhotos(indexPath: IndexPath) -> Bool {
		return !isLoadingPhotos && numberOfPages != currentPageNumber && shouldLoadExtraPage(indexPath: indexPath)
  }
  
  private func updateUI(_ photoList: PhotoList?,_ errorType:  RequestErrorType = .noError) {
    guard errorType == .noError else {
			let errorName = String(reflecting: errorType)
			NSLog("[Error] There was a problem when trying to load the PhotoList %@", errorName)
			return
		}
		guard let photoList = photoList else {
			NSLog("[Error] There was a problem when trying to load the PhotoList, Data is nil")
			return
		}
		let photos = photoList.photos
		let photoViewModels = photos.compactMap({ PhotoViewModel(photo: $0) })

		allPhotos.append(contentsOf: photoViewModels)
		numberOfPages = photoList.pages
		currentPageNumber = photoList.page
		
		controller?.collectionView.reloadData()
    isLoadingPhotos = false
  }
  
	private func updatePhotoWithDetails(_ photoDetailsList: PhotoDetailsList?,_ errorType: RequestErrorType, _ identifier: String?) {
		guard errorType == .noError else {
			let errorName = String(reflecting: errorType)
			NSLog("[Error] There was a problem when trying to load the PhotoDetailsList %@", errorName)
			return
		}
		
		guard let photoDetailsList = photoDetailsList else {
			NSLog("[Error] There was a problem when trying to load the PhotoDetailsList, Data is nil")
			return
		}
		
		guard let identifier = identifier else {
			NSLog("[Error] There was a problem when trying to load the PhotoDetailsList, Identifier not found")
			return
		}
		
		DispatchQueue.global().async {
			guard let photoDetails = photoDetailsList.details.first(where: { $0.label == "Large Square" }) else { return }
			
			guard let photo = self.allPhotos.first(where: { $0.photoId == identifier }) else { return }
			
			DispatchQueue.main.async {
				photo.addPhotoDetails(photoDetails: photoDetails)
			}
		}
	}
	
  private func showErrorMessage(forType errorType: RequestErrorType) {
		guard let controller = controller else { return }
    ErrorMessenger.showErrorMessage(forType: errorType, onViewController: controller)
  }
  
	private func resetGallery() {
		allPhotos = []
		currentTag = ""
		currentPageNumber = 0
		numberOfPages = 0
	}
  
  func photo(ofIndexPath indexPath: IndexPath) -> PhotoViewModel {
    return allPhotos[indexPath.row]
  }
}
