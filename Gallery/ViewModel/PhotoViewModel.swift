//
//  PhotoViewModel.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class PhotoViewModel {
	
	private let photo: Photo
	private var photoDetails: PhotoDetails?
	
	var photoId: String {
		return photo.id
	}
	
	var photoURL: URL? {
		guard let stringURL = photoDetails?.source else { return nil }
		return URL(string: stringURL)
	}
	
	var photoSize: CGSize {
		guard let photoDetails = self.photoDetails else {
			return CGSize(width: 150, height: 150)
		}
		let width = photoDetails.width
		let height = photoDetails.height
		
		return CGSize(width: width, height: height)
	}
	
	init(photo: Photo) {
		self.photo = photo
	}
	
	func addPhotoDetails(photoDetails: PhotoDetails) {
		self.photoDetails = photoDetails
	}
}
