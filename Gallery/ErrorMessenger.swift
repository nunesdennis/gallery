//
//  ErrorMessenger.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class ErrorMessenger {
  
  class func showErrorMessage(forType errorType: RequestErrorType, onViewController controller: UIViewController) {
    var title: String!
    var message: String!

    switch errorType {
    case .connectionError:
      title = MessageGuide.connectionErrorTitle
      message = MessageGuide.connectionErrorMessage
    default:
      title = MessageGuide.genericErrorTitle
      message = MessageGuide.genericErrorMessage
    }

    let alertAction = UIAlertAction(title: "Ok", style: .default)
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(alertAction)

    controller.present(alertController, animated: true)
  }
}
