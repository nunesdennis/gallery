//
//  photoCollectionViewCell.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var galleryImageView: GalleryImageView!
	
	func prepareCell(withPhoto photo: PhotoViewModel) {
		galleryImageView.image = UIImage(named: "placeholder")
		guard let url = photo.photoURL else { return }
		galleryImageView.loadImage(ofURL: url)
	}
}
