//
//  UrlGuide.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

enum UrlGuide {
	
	static var photoSearchStringURL: String = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9cc014fa76b098f9e82f1c288379ea1&format=json&nojsoncallback=1"
	
	static var photoGetDetailStringURL: String = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=f9cc014fa76b098f9e82f1c288379ea1&format=json&nojsoncallback=1"
}
