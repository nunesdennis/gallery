//
//  PhotoList.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct PhotoList: Codable {
	
	let page: Int64
	let pages: Int64
	let photos: [Photo]
	let total: String
	
	enum CodingKeys: String, CodingKey {
		case page
		case pages
		case photos = "photo"
		case total
	}
}
