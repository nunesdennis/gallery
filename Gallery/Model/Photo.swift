//
//  Photo.swift
//  Gallery
//
//  Created by Dennis Nunes on 23/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct Photo: Codable {
	
	let id: String
	let title: String
}
