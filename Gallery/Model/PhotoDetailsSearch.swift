//
//  PhotoDetailsSearch.swift
//  Gallery
//
//  Created by Dennis Nunes on 24/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct PhotoDetailsSearch: Codable {
	
	let photoDetailsList: PhotoDetailsList
	
	enum CodingKeys: String, CodingKey {
		case photoDetailsList = "sizes"
	}
}
