//
//  PhotoDetailsList.swift
//  Gallery
//
//  Created by Dennis Nunes on 24/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct PhotoDetailsList: Codable {
	
	let details: [PhotoDetails]
	
	enum CodingKeys: String, CodingKey {
		case details = "size"
	}
}
