//
//  PhotoDetails.swift
//  Gallery
//
//  Created by Dennis Nunes on 24/11/19.
//  Copyright © 2019 Dennis Nunes. All rights reserved.
//

import Foundation

struct PhotoDetails: Codable {
	
	let label: String
	let width: Int
	let height: Int
	let source: String
}
